﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CamadaDeApresentacao.Models;

namespace CamadaDeApresentacao.Controllers
{
    public class AlunoController : Controller
    {
        public ActionResult Editar()
        {
            Aluno aluno = new Aluno
            {
                AlunoID = 1,
                Nome = "Jonas Hirata",
                Email = "jonas@k19.com.br"
            };

            return View(aluno);
        }

        public ActionResult Lista()
        {
            ICollection<Aluno> lista = new List<Aluno>();

            for (int i = 0; i < 5; i++)
            {
                Aluno a = new Aluno
                {
                    AlunoID = i,
                    Nome = "Aluno " + i,
                    Email = "E-mail " + i
                };

                lista.Add(a);
            }

            return View(lista);
        }

        public ActionResult Detalhes()
        {
            Aluno a = new Aluno
            {
                AlunoID = 1,
                Nome = "Jonas Hirata",
                Email = "jonas@k19.com.br"
            };

            return View(a);
        }
    }
}
